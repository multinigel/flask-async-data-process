import io


def test_file_upload(test_client):
    response = test_client.post(
        '/',
        data={
            'file': (io.BytesIO(b"Tkn435JFIRKTknczxcxz\nTkn435AAAAATknczxc',\nczxczxc"), 'file.txt')
        },
        follow_redirects=True,
        content_type='multipart/form-data'
    )

    assert response.status_code == 200
    assert response.json == {'Tkn435AAAAATkn': 1, 'Tkn435JFIRKTkn': 1}
