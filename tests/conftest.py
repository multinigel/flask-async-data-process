import pytest
from app import app


@pytest.fixture()
def test_client():
    # app.config.update({
    #     "TESTING": True,
    # })

    # other setup can go here

    # yield app

    # clean up / reset resources here
    flask_app = app
    testing_client = flask_app.test_client()
    ctx = flask_app.app_context()
    ctx.push()
    yield testing_client
    ctx.pop()


# @pytest.fixture()
# def client(app):
#     return app.test_client()
#
#
# @pytest.fixture()
# def runner(app):
#     return app.test_cli_runner()
