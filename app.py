import json
import os
import re
import uuid
from collections import Counter
from redis import Redis

from flask import Flask, request
from celery import Celery

from dir_scanner import search_file


app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = '/code'
redis = Redis(host='redis', port=6379)
celery = Celery(app.name, broker='redis://redis:6379/0')
celery.conf.update(app.config)


@app.route('/', methods=['POST'])
def scan_file():
    file = request.files['file']
    result = re.findall("Tkn[0-9]{3}[A-Z]{5}Tkn", file.read().decode(errors='ignore'))
    return Counter(result)


@celery.task
def scan_file_task(file_path, token):
    _, result = search_file(file_path)
    redis[token] = json.dumps(Counter(result))


@app.route('/scan_async', methods=['POST'])
def scan_file_async():
    token = str(uuid.uuid4())
    file_path = os.path.join(app.config['UPLOAD_FOLDER'], token)

    request.files['file'].save(file_path)
    scan_file_task.apply_async(kwargs={"file_path": file_path, "token": token})
    return {'token': token}


@app.route('/scan_async/<string:token>', methods=['GET'])
def fetch_async_result(token):
    return redis[token]


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
