1. run `docker-compose run --rm --entrypoint "python dir_scanner.py" web` to simulate multiprocessing recusive file parsing
2. run `docker-compose run --rm --entrypoint "python -m pytest" web` to test file upload with response or `docker-compose up` to test manually
3. run `docker-compose up`, send file to `/scan_file_async`, get token and `/scan_async/<string:token>` get result

