import csv
import json
import os
import re
from collections import Counter
from operator import itemgetter
import multiprocessing as mp


def search_file(abs_path):
    with open(abs_path, 'rb') as f:
        text = f.read().decode(errors='ignore')
    result = re.findall("Tkn[0-9]{3}[A-Z]{5}Tkn", text)
    return abs_path, result


def analyze_firmware(directory_path, csv_output_path):
    files = []
    csv_data = []
    total_token_findings = []

    # collect file paths
    for root, _, filenames in os.walk(directory_path):
        for filename in filenames:
            files.append(os.path.join(root, filename))

    # run search on each one
    with mp.Pool(processes=4) as pool:
        search_result = pool.map(search_file, files)

    # group result
    for result in search_result:
        abs_path, token_list = result
        rel_path = abs_path[len(directory_path):]
        counter = Counter(token_list)

        total_token_findings.extend(token_list)

        for token, count in counter.items():
            csv_data.append((rel_path, token, count))

    csv_data.sort(key=itemgetter(0, 2, 1))

    # write result to file
    with open(csv_output_path, 'w+') as f:
        writer = csv.writer(f)
        writer.writerows(csv_data)

    total_findings = Counter(total_token_findings)
    print(json.dumps(total_findings))


if __name__ == '__main__':
    os.makedirs('/tmp/_rootdir/_sub', exist_ok=True)
    with open('/tmp/_rootdir/f1.txt', 'w+') as f:
        f.writelines((
            'Tkn435JFIRKTknczxcxz',
            'Tkn435AAAAATknczxc',
            'czxczxc',
        ))

    with open('/tmp/_rootdir/_sub/f1.txt', 'w+') as f:
        f.writelines((
            'Tkn435AAAAATknczxcTkn435CCCCCTknczxc',
            'Tkn435BBBBBTknczxc',
            'Tkn435BBBBBTknczxc',
            'czxcxc',
        ))

    analyze_firmware('/tmp/_rootdir', '/tmp/f.csv')
